#ifndef Live_Music_h
#define Live_Music_h

class Live_Music
{
public:
  int  seatsTotal;
  int seatsBooked ;
  
  void musicDetails();
  void addBooking();
  void cancelBooking();

};

#endif