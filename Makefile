CXX = g++
CXXFLAGS = −g −Wall −Wextra −Wpedantic
program : main.cpp Film.o Stand_Up.o Event.o Live_Music.o
	$(CXX) $(CXXFLAGS) −o program main.cpp Film.o Stand_Up.o Event.o Live_Music.o
Film.o : Film.cpp Film.h
	$(CXX) $(CXXFLAGS) −c Film.cpp
Stand_Up.o : Stand_Up.cpp Stand_Up.h
	$(CXX) $(CXXFLAGS) −c Stand_Up.cpp
Event.o : Event.cpp Event.h
	$(CXX) $(CXXFLAGS) −c Event.cpp
Live_Music.o : Live_Music.cpp Live_Music.h
	$(CXX) $(CXXFLAGS) −c Live_Music.cpp

PHONY : clean
clean :
	rm ∗ . o	
	rm program
